/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

/*********************************************************************************
  AlignedDynArray.icc  -  description
  -------------------------------------------------
 begin                : 26th November 2019
 author               : amorley, Christos
 decription           : Dynamic array fullfilling alignment requirements
 *********************************************************************************/

namespace GSFUtils {

template<typename T, size_t Alignment>
inline AlignedDynArray<T, Alignment>::AlignedDynArray(size_t n)
  : m_buffer(nullptr)
  , m_size(n)
{
  const size_t bufferSize = n * sizeof(T);
  m_buffer = static_cast<T*>(std::aligned_alloc(Alignment, bufferSize));
  std::uninitialized_default_construct(m_buffer,m_buffer+m_size);
}

template<typename T, size_t Alignment>
inline AlignedDynArray<T, Alignment>::AlignedDynArray(size_t n, const T& value)
  : m_buffer(nullptr)
  , m_size(n)
{
  const size_t bufferSize = n * sizeof(T);
  m_buffer = static_cast<T*>(std::aligned_alloc(Alignment, bufferSize));
  std::uninitialized_fill(m_buffer,m_buffer+m_size,value);
}

template<typename T, size_t Alignment>
inline AlignedDynArray<T, Alignment>::AlignedDynArray(AlignedDynArray&& other)
  : m_buffer(nullptr)
  , m_size(0)
{
  // copy over other
  m_buffer = other.m_buffer;
  m_size = other.m_size;
  // set other to invalid
  other.m_buffer = nullptr;
  other.m_size = 0;
}

template<typename T, size_t Alignment>
inline AlignedDynArray<T, Alignment>&
AlignedDynArray<T, Alignment>::operator=(AlignedDynArray&& other)
{

  if (this != &other) {
    // cleanup this object
    cleanup();
    // copy over other
    m_buffer = other.m_buffer;
    m_size = other.m_size;
    // set other to invalid
    other.m_buffer = nullptr;
    other.m_size = 0;
  }
  return *this;
}

template<typename T, size_t Alignment>
inline AlignedDynArray<T, Alignment>::~AlignedDynArray()
{
  cleanup();
}

template<typename T, size_t Alignment>
inline AlignedDynArray<T, Alignment>::operator T*()
{
  return m_buffer;
}

template<typename T, size_t Alignment>
inline AlignedDynArray<T, Alignment>::operator const T*() const
{
  return m_buffer;
}

template<typename T, size_t Alignment>
inline T& AlignedDynArray<T, Alignment>::operator[](const std::size_t pos)
{
  return m_buffer[pos];
}

template<typename T, size_t Alignment>
inline const T& AlignedDynArray<T, Alignment>::operator[](const std::size_t pos) const
{
  return m_buffer[pos];
}

template<typename T, size_t Alignment>
inline std::size_t
AlignedDynArray<T, Alignment>::size() const
{
  return m_size;
}

template<typename T, size_t Alignment>
inline void
AlignedDynArray<T, Alignment>::cleanup()
{
  if (m_buffer) {
    for (std::size_t pos = 0; pos < m_size; ++pos) {
      m_buffer[pos].~T();
    }
    std::free(m_buffer);
  }
}

} // namespace GSFUtils

